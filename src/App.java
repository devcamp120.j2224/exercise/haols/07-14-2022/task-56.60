import com.devcamp.S50.Task5660.DateClass;

public class App {
    public static void main(String[] args) throws Exception {
        DateClass date1 = new DateClass(20, 9, 2011);
        DateClass date2 = new DateClass(14, 7, 2022);
        System.out.println(date1.toString());
        System.out.println(date2.toString());
    }
}
